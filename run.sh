kubectl apply -f postgres-storage.yaml

kubectl apply -f all-secrets.yaml

kubectl apply -f postgres-configmap.yaml
kubectl apply -f postgres-deployment.yaml
kubectl apply -f postgres-service.yaml

kubectl apply -f redmine-configmap.yaml
kubectl apply -f redmine-deployment.yaml
kubectl apply -f redmine-service.yaml

