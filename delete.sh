kubectl delete svc redmine-service
kubectl delete svc postgres-service

kubectl delete deployment redmine
kubectl delete deployment postgres

kubectl delete configmap postgres-config
kubectl delete configmap redmine-config

kubectl delete secret all-secrets

kubectl delete persistentvolumeclaim postgres-pv-claim
kubectl delete persistentvolume postgres-pv-volume
